﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Mobilne_Projekt
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MediaPlayer mediaPlayer = new MediaPlayer();
        private int urlsCount = 0, filesCount = 0;
        private int curTrack = 0, curtUri = 0;

        private ObservableCollection<Uri> uris = new ObservableCollection<Uri>();
        private ObservableCollection<String> filesTokens = new ObservableCollection<String>();
        private ObservableCollection<StorageFile> files = new ObservableCollection<StorageFile>();
        private PivotItem pivot = null;
        private int view = 0;


        public MainPage()
        {
            this.InitializeComponent();

            //ToDO:
            //poprava volume
            //sprawdzanie polaczenia z internetem

            //czytanie z pamieci url i plikow
            redUrls();
            redTokenFolder();

            //przypisanie do list
            listViewUrl.ItemsSource = uris;
            listViewFile.ItemsSource = files;
            volumeSlider.Value = (Double)mediaPlayer.Volume;

            //Ustawienie rodzaju medium
            mediaPlayer.AudioCategory = MediaPlayerAudioCategory.Media;

            //obsluga przyciskow systemowych
            mediaPlayer.CommandManager.NextBehavior.EnablingRule = MediaCommandEnablingRule.Always;
            mediaPlayer.CommandManager.NextReceived += CommandManager_NextReceived;
            mediaPlayer.CommandManager.PreviousBehavior.EnablingRule = MediaCommandEnablingRule.Always;
            mediaPlayer.CommandManager.PreviousReceived += CommandManager_PreviousReceived;

            //obsluga zmiany na nastepny po koncu
            mediaPlayer.MediaEnded += MediaPlayer_mediaEnd;
            
            //obsluga zmiany widoku
            rootPivot.SelectionChanged += RootPivot_selectionChanged;

            //obsluga podwojnego tapniecia dla url
            listViewUrl.DoubleTapped += ListViewUrl_doubleTapped;

            //obsluga podwojnego tapniecia dla plikow
            listViewFile.DoubleTapped += ListViewFile_doubleTapped;
        }
        private async void CommandManager_NextReceived(MediaPlaybackCommandManager sender, MediaPlaybackCommandManagerNextReceivedEventArgs args)
        {
            if (curTrack < filesCount - 1)
            {
                curTrack = curTrack + 1;

                await SetNewFile();
            }
        }
        private async void CommandManager_PreviousReceived(MediaPlaybackCommandManager sender,MediaPlaybackCommandManagerPreviousReceivedEventArgs args)
        {
            if (curTrack > 0)
            {
                curTrack = curTrack - 1;

                await SetNewFile();
            }
        }
        private async void MediaPlayer_mediaEnd(MediaPlayer sender, object e)
        {
            if (curTrack < filesCount - 1)
            {
                curTrack = curTrack + 1;

                await SetNewFile();
            }
        }
        private void VolumeSlider_valueChanged(object sender, RoutedEventArgs e)
        {
            Slider slider = sender as Slider;
            if (slider != null)
            {
                mediaPlayer.Volume = (Double)slider.Value;
            }
        }
        private void RootPivot_selectionChanged(object sender, RoutedEventArgs e)
        {
            pivot = (PivotItem)(sender as Pivot).SelectedItem;
            switch (pivot.Header.ToString())
            {
                case "Urls":
                    view = 0;
                    break;
                case "Music":
                    view = 1;
                    break;
                default:
                    TextBlockContent.Text = "Problem z pivotem";
                    break;
            }
        }
        private void Play_click(object sender, RoutedEventArgs e)
        {
            if (listViewUrl.SelectedItem != null || listViewFile.SelectedItem != null)
            {
                if (view == 0)
                {
                    curtUri = listViewUrl.SelectedIndex;
                    mediaPlayer.Source = MediaSource.CreateFromUri((Uri)listViewUrl.SelectedItem);
                    mediaPlayer.Play();
                    TextBlockContent.Text = listViewUrl.SelectedItem.ToString();
                }
                else if (view == 1)
                {
                    curTrack = listViewFile.SelectedIndex;
                    StorageFile currentTrack = (StorageFile)listViewFile.SelectedItem;
                    mediaPlayer.Source = MediaSource.CreateFromStorageFile(currentTrack);
                    mediaPlayer.Play();
                    TextBlockContent.Text = currentTrack.Name.ToString();
                }
            }
            else
            {
                TextBlockContent.Text = "Wybierz element z listy lub dodaj";
            }
        }
        private void Pause_click(object sender, RoutedEventArgs e)
        {
            mediaPlayer.Pause();
        }
        private void ListViewUrl_doubleTapped(object sender, RoutedEventArgs e)
        {
            curtUri = listViewUrl.SelectedIndex;
            TextBlockContent.Text = listViewUrl.SelectedItem.ToString();
            mediaPlayer.Source = MediaSource.CreateFromUri((Uri)listViewUrl.SelectedItem);
            mediaPlayer.Play();
        }
        private void ListViewFile_doubleTapped(object sender, RoutedEventArgs e)
        {
            curTrack = listViewFile.SelectedIndex;
            StorageFile currenTrack = (StorageFile)listViewFile.SelectedItem;
            mediaPlayer.Source = MediaSource.CreateFromStorageFile(currenTrack);
            mediaPlayer.Play();
            TextBlockContent.Text = currenTrack.DisplayName;
        }
        private async void OpenLocal_click(object sender, RoutedEventArgs e)
        {
            var picker = new FileOpenPicker();
            picker.ViewMode = PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = PickerLocationId.MusicLibrary;
            picker.FileTypeFilter.Add(".mp3");

            var pickedFiles = await picker.PickMultipleFilesAsync();
            if (files != null)
            {
                // Application now has read/write access to all contents in the picked folder
                // (including other sub-folder contents)
                foreach (StorageFile file in pickedFiles)
                {
                    StorageApplicationPermissions.FutureAccessList.AddOrReplace(file.Name, file);
                    filesTokens.Add(file.Name);
                    files.Add(file);
                }
                savTokenFolder();
                filesCount = files.Count;
            }
            else
            {
                TextBlockContent.Text = "Operation cancelled.";
            }
        }
        private void AddUrl_click(object sende, RoutedEventArgs e)
        {
            Uri uriResult;
            if (Uri.TryCreate(TextBoxUrl.Text, UriKind.Absolute, out uriResult))
            {
                uris.Add(uriResult);
                FlyoutUrl.Hide();

                urlsCount = uris.Count;
                savUrls();
            }
            else
            {
                TextBlockContent.Text = "Bledny link.";
            }
        }
        private void Shuffle_click(object sender, RoutedEventArgs e)
        {
            ShuffleObservable(files);
        }
        private void Back_click(object sender, RoutedEventArgs e)
        {
            if (view == 0)
            {
                if (curtUri > 0)
                {
                    curtUri = curtUri - 1;
                    listViewUrl.SelectedIndex = curtUri;
                    mediaPlayer.Source = MediaSource.CreateFromUri((Uri)listViewUrl.SelectedItem);
                    mediaPlayer.Play();
                    TextBlockContent.Text = listViewUrl.SelectedItem.ToString();
                }
            }
            else if (view == 1)
            {
                if (curTrack > 0)
                {
                    curTrack = curTrack - 1;
                    listViewFile.SelectedIndex = curTrack;
                    StorageFile currenTrack = (StorageFile)listViewFile.SelectedItem;
                    mediaPlayer.Source = MediaSource.CreateFromStorageFile(currenTrack);
                    mediaPlayer.Play();
                    TextBlockContent.Text = currenTrack.Name.ToString();
                }
                else
                {
                    
                }
            }
        }
        private void Forwar_click(object sender, RoutedEventArgs e)
        {
            if (view == 0)
            {
                if (curtUri < urlsCount - 1)
                {
                    curtUri = curtUri + 1;
                    listViewUrl.SelectedIndex = curtUri;
                    mediaPlayer.Source = MediaSource.CreateFromUri((Uri)listViewUrl.SelectedItem);
                    mediaPlayer.Play();
                    TextBlockContent.Text = listViewUrl.SelectedItem.ToString();
                }
            }
            else if (view == 1)
            {
                if (curTrack < filesCount - 1)
                {
                    curTrack = curTrack + 1;
                    listViewFile.SelectedIndex = curTrack;
                    StorageFile currenTrack = (StorageFile)listViewFile.SelectedItem;
                    mediaPlayer.Source = MediaSource.CreateFromStorageFile(currenTrack);
                    mediaPlayer.Play();
                    TextBlockContent.Text = currenTrack.Name.ToString();
                }
            }
        }
        private void Delete_click(object sender, RoutedEventArgs e)
        {
            if (view == 0)
            {
                if (uris.Remove((Uri)listViewUrl.SelectedItem))
                {
                    savUrls();
                    urlsCount = uris.Count;
                }
            }
            else if (view == 1)
            {
                //nalezy usunac z tzrech list FutureAccess, Files, Tokens, FutureAccess musi byc pierwsza 
                listViewFile.SelectedIndex = curTrack;
                StorageFile selectedTrack = (StorageFile)listViewFile.SelectedItem;
                StorageApplicationPermissions.FutureAccessList.Remove(selectedTrack.Name);
                filesTokens.Remove(selectedTrack.Name);
                files.Remove(selectedTrack);

                savTokenFolder();
                filesCount = files.Count;
            }
        }

        private async void redUrls()
        {
            var Serializer = new DataContractSerializer(typeof(ObservableCollection<Uri>));
            
            if(await ApplicationData.Current.LocalFolder.TryGetItemAsync("Urls") != null){
                using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("Urls"))
                {
                    uris = (ObservableCollection<Uri>)Serializer.ReadObject(stream);
                }
                urlsCount = uris.Count;
            }
        }
        private async void savUrls()
        {
            //zapisanie do pliku
            StorageFile userUrlFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Urls", CreationCollisionOption.ReplaceExisting);
            IRandomAccessStream raStream = await userUrlFile.OpenAsync(FileAccessMode.ReadWrite);
            using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
            {
                // Serialize the Session State. 
                DataContractSerializer serializer = new DataContractSerializer(typeof(ObservableCollection<Uri>));

                serializer.WriteObject(outStream.AsStreamForWrite(), uris);

                await outStream.FlushAsync();
                outStream.Dispose(); 
                raStream.Dispose();
            }
        }

        private async void redTokenFolder()
        {
            var Serializer = new DataContractSerializer(typeof(ObservableCollection<String>));

            if (await ApplicationData.Current.LocalFolder.TryGetItemAsync("FilesTokens") != null)
            {
                using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("FilesTokens"))
                {
                    filesTokens = (ObservableCollection<String>)Serializer.ReadObject(stream);
                }

                foreach(var token in filesTokens)
                {
                    files.Add(await StorageApplicationPermissions.FutureAccessList.GetFileAsync(token));
                }

                filesCount = files.Count;
            }
        }
        private async void savTokenFolder()
        {
            //zapisanie do pliku
            StorageFile userTokenFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("FilesTokens", CreationCollisionOption.ReplaceExisting);
            IRandomAccessStream raStream = await userTokenFile.OpenAsync(FileAccessMode.ReadWrite);
            using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
            {
                // Serialize the Session State. 
                DataContractSerializer serializer = new DataContractSerializer(typeof(ObservableCollection<String>));

                serializer.WriteObject(outStream.AsStreamForWrite(), filesTokens);

                await outStream.FlushAsync();
                outStream.Dispose();
                raStream.Dispose();
            }
        }

        private void ShuffleObservable<T>(ObservableCollection<T> target)
        {
            ObservableCollection<T> intermediate = new ObservableCollection<T>();
            foreach (T item in target)
                intermediate.Add(item);

            target.Clear();

            Random R = new Random();
            while (intermediate.Count > 0)
            {
                int index = R.Next(0, intermediate.Count - 1);
                target.Add(intermediate[index]);
                intermediate.RemoveAt(index);
            }
        }

        private async Task SetNewFile()
        {
            //listViewFile.SelectedIndex = curTrack;
            StorageFile currentTrack = files[curTrack];
            IRandomAccessStream stream = null;
            stream = await currentTrack.OpenAsync(FileAccessMode.Read);
            mediaPlayer.Source = MediaSource.CreateFromStream(stream, currentTrack.ContentType);
            mediaPlayer.Play();
            //TextBlockContent.Text = currentTrack.Name.ToString();
        }
    }
}
